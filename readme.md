# Spike Report

## Core 1 - Setting up your Development Environment

### Introduction

This Spike is designed to improve skills, to students unfamiliar with Unreal Engine, C++ and git (using SourceTree, and the GitFlow method). Therefore, allowing students to know how to set up their enviroment to support later work in the coming weeks.

### Goals

1. Revise how to properly use git, including gitignore, and Gitflow (there are Resources available on Blackboard)
2. Set up an empty git repository, local and/or remote (using bitbutcket)
    * A. Set up a suitable gitignore for Unreal Engine 4 and commit it
    * B. Set up the UE4 project as detailed above and commit it

### Personnel

* Primary - Geordie

### Technologies, Tools, and Resources used

* SourceTree
* Unreal Engine
* Visual Studio Code
* Blackboard Refreasher content from week 1 SGD240.
* [YouTube video](https://www.youtube.com/watch?v=JbyawFZ1Rz4 "SourceTree & BitBucket for UE4 / Unreal Engine 4 by Dev Enabled") by Dev Enabled explaining how to get the repository set up
* [Simple .gitignore](https://github.com/DevEnabled/UE4_GitIgnoreTemplate/blob/master/.gitignore "gitignore file from Dev Enabled") given by Dev Enabled as an example in his video
* [advanced .gitignore](https://github.com/github/gitignore/blob/master/UnrealEngine.gitignore "Example gitignore for UE4") for unity engine
* [Article](https://www.attosol.com/undo-in-git-using-sourcetree/ "Undo in Git using SourceTree") explaining source tree, clarifying ways to deal with accidental branches and linking to next useful resource
* [Article](https://jarrodspillers.com/blog/git/2009-08-19-git-merge-vs-git-rebase-avoiding-rebase-hell/ "git merge vs git rebase: Avoiding Rebase Hell") that just simplifies concepts around Rebasing and merging, also giving examples in which cases to use them and benefits for both
  
### Tasks Undertaken

1. Formatted a correct UE4 .gitignore file (Found resources from [Simple .gitignore](https://github.com/DevEnabled/UE4_GitIgnoreTemplate/blob/master/.gitignore "gitignore file from Dev Enabled") and [advanced .gitignore](https://github.com/github/gitignore/blob/master/UnrealEngine.gitignore "Example gitignore for UE4") to create one for UE4).
2. Created a Repository in bitbucket (This [YouTube video](https://www.youtube.com/watch?v=JbyawFZ1Rz4 "SourceTree & BitBucket for UE4 / Unreal Engine 4 by Dev Enabled") explains how to set up the repository and set up SourceTree if you haven't already)
3. Submitted a gitignore to set up the master branch.
4. Committed both readme.md and spikeplan files.
5. Created a basic UE4 project and tested it with a commit to a repository.
6. Removed basic UE4 project from the main repository.
7. Created a FPS UE4 project without the starter items.

### What we found out

Very easy to create a new branch when trying to remove an accidental commit. A new branch became difficulte to remerge into the master branch thus making the repository busy and complicated to navigate. The solution became to start over as it became too complicated to solve, and instead opted for a cleaner set of commits with the new repository.

Will need to go through and figure out how to clean unnecessary commits as this situation will certainly happen again, and resetting the repository will be an unviable option with bigger projects with multiple branches and commits.

Gitignore files are much easier to set up if you are aware of what you intend on removing from the project. A template is a good place to start, and gives an idea of what you are excluding.

With not using a proper gitignore file it will not remove properties unnecessary with UE4 projects.

### [Optional] Open Issues/Risks

Null

### [Optional] Recommendations

Ensure you have a proper .gitignore file to reduce unnecessary files and be certain of the commits you are taking.

Consider where you place your .gitignore file, as placing it in the folder above the project, the gitignore can't find the components to ignore. So be sure to place it in the same subfolder as the project or point your gitignore to the subfolder of the project.
