// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "FPS_BasicsGameMode.generated.h"

UCLASS(minimalapi)
class AFPS_BasicsGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AFPS_BasicsGameMode();
};



