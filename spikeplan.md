# Spike Plan

## Core 1 - Setting up Your Development Environment

### Context

This class uses Unreal Engine C++, and git(using SourceTree, and the gitflow method).
Therefore, it is required that we know how to set up the environment to support later work.

### Gap

1. We do not know ho to create Unreal Engine Projects
2. We do not know how to create Unreal Engine C++ Projects
3. We have limited prior exposure to git, and need to figure out how to use it with Unreal Engine
4. We haven't used a formal git workflow like Gitflow before, so don't know how to use it

### Goals / Deliverables

1. The spike report should answer each of the Gap questions
2. A git repository, which contains:
   * a. A proper gitignore file for Unreal Engine as the first commit
   * b. The Unreal Engine First Person Shooter C++ project, without Starter Materials

### Dates

Planned start date: week 1
Deadline: week 2

### Planning Notes

1. Revise how to properly use git, including gitignore, and Gitflow (there are Resources available on Blackboard)
2. Set up an empty git repository, local and/or remote (using bitbutcket)
3. Set up a suitable gitignore for Unreal Engine 4 and commit it
4. Set up the UE4 project as detailed above and commit it
